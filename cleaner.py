import glob
import json
import sys

ebook_titles = {}

def format_line(line):
    line = line.strip()
    line = line.replace("&rsquo;", "'")
    if line.count("; ") is not 2:
        temp = line.split("; ")
        line = str(temp[0]) + "; " + str(temp[1:-2]) + "; " + str(temp[-1])
    return line


def is_magazine(title):
    magazines = ["magazine", "wired usa", "penthouse", "android advisor", "playboy", "popular mechanics", "the economist"]
    for name in magazines:
        if name in title.lower():
            return True
    return False

def parse_line(line):
    global ebook_titles
    line = format_line(line)
    # score is 0 for 100, 1 for 99, needs to be fixed
    try:
        score, title, date = tuple(line.split("; "))
    except:
        print line
    score = -(int(score)) + 100
    if ebook_titles.has_key(title) == False:
        ebook_titles[title] = {"total_score": 0, "date_score": [], "genre": None, "magazine": False}
    ebook_titles[title]["total_score"] += score
    ebook_titles[title]["magazine"] = is_magazine(title)
    ebook_titles[title]["date_score"].append((date, score))

def run():
    global ebook_titles
    file_list = glob.glob(r"./data/*.txt")
    file_lines = None
    for file_path in file_list:
        try:
            file_lines = open(file_path, 'r').readlines()
        except:
            print "ERROR: reading file/lines: %s " % (file_path)
            continue
        for line in file_lines:
            parse_line(line)

    with open('pb-ebooks.json', 'w') as fp:
        json.dump(ebook_titles, fp)

run()
